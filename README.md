# mtx_pricing_tool

## Purpose

A simple tool for checking a pricing XML

To check whether or not the dependancies are satisfied and output the catalog in a yaml document.

Accepts a single mtx pricing file and may optionally output a yaml file

## Installation.

Written for python 3

Use a venv to install the dependancies

* `python3 -m venv venv`
* `source venv/bin/activate`
* `pip install -r requirements.txt`
* `./main.py --help`

## Usage

usage: main.py [-h] -i INPUT [-o OUTPUT] [-v VERBOSITY]

Process a pricing file to a yaml output.

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        location of the input file pricing xml.
  -o OUTPUT, --output OUTPUT
                        location to output the new yaml file.
  -v VERBOSITY, --verbosity VERBOSITY
                        Logging level for applicati
                        on
