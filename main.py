#!/usr/bin/env python

import argparse
from bs4 import BeautifulSoup
import bs4
import logging
import os
import oyaml as yaml

logging.basicConfig(level=logging.INFO)

main_logger = logging.getLogger("matrixx:pricing-tool:main")
logger = logging.getLogger('matrixx:pricing-tool:parser')


OUTPUT_DIRECTORY = "output"


class DependancyManager():
    def __init__(self):
        self.required = set()
        self.supplied = set()

    def find_unsatified_depedancies(self):
        main_logger.info("Dependancies {}".format(self.required))
        main_logger.info("Supplied {}".format(self.supplied))
        return self.required.difference(self.supplied)

    def __repr__(self):
        return "Contents of manager {} {}".format(
            self.supplied, self.required)


manager = DependancyManager()


def __find_eldest_child(revision_node_list):
    """
    For a given XML NOde of RevArray find the
    eldest revision.

    -- Pirvatae method, this sorts though the Rev nodes
    finds the eldest - assuming there aren"t ordered.
    And returns it.
    """
    logger.debug("Curent node list {}".format(revision_node_list))
    current_revision_node = None
    current_revision_number = 0
    for temp_revision in revision_node_list:
        logger.debug("Iterating over {}".format(temp_revision))
        revision_number = temp_revision.find("Rev")
        logger.debug("Rev found {}".format(revision_number))
        temp_revision_number = int(revision_number.text)
        if temp_revision_number > current_revision_number:
            current_revision_number = temp_revision_number
            current_revision_node = temp_revision
    return current_revision_node


def find_eldest_child(catalog_item):
    """
    For a given XML NOde of RevArray find the
    eldest revision.
    """
    rev_array = catalog_item.find("RevArray")
    if rev_array:
        logger.debug("Found revision array")
        logger.debug("Revision Array {}".format(rev_array.prettify()))
        just_nodes = [x for x in rev_array.contents if type(
            x) is bs4.element.Tag]
        logger.debug("Only XML Nodes {}".format(just_nodes))
        if len(just_nodes) == 1:
            logger.debug("Returning only node")
            return just_nodes[0]
        else:
            logger.info("More than one revision found")
            return __find_eldest_child(just_nodes)
    else:
        logger.error("Can't find revision array")
        raise Exception("Can't find revision array")


def __get_meta_(node, find_name, populate_dict):
    """
    Get meta data from a given Catalog item xml node.
    Populates the popualte dict.
    """
    value_dict = {}
    for item in node.find_all("MtxPricingMetadata"):
        name = item.find("Name").text
        if name == find_name:
            value = item.find("Value").text
            logger.debug("Value of {} metadata {}".format(find_name, value))
            if populate_dict:
                value_dict = make_dictionary_from_meta_string(
                    value)
                populate_dict[find_name] = value_dict
    logger.debug("Populated dict with meta {}".format(populate_dict))
    return value_dict


def get_provides(node, populate={}):
    """
    Get meta string value for key provides
    """
    return __get_meta_(node, "provides", populate)


def get_requires(node, populate={}):
    """
    Get meta string value for key requires
    """
    return __get_meta_(node, "requires", populate)


def get_excludes(node, populate={}):
    """
    Get meta string value for key excludes
    """
    return __get_meta_(node, "excludes", populate)


def make_dictionary_from_meta_string(meta_string):
    """
    For a string of meta data, dot seperated generated a dict.

    i.e
    PurchasedOfferArray.provides.voice ->
    {"PurchasedOfferArray": {"provides": "voice"}}

    """
    meta_values = [child.strip() for child in meta_string.split(",")]
    for index, child in enumerate(meta_values):
        logger.debug("Meta string child {}".format(child))
        parent = {}
        current_level = parent
        previous_level = None
        input_split = child.split(".")
        keys, value = input_split[:-1], input_split[-1]
        if not keys:
            continue
        for key in keys:
            current_level[key] = {}
            previous_level = current_level
            current_level = current_level[key]
        if previous_level:
            previous_level[key] = value
        else:
            current_level[key] = value
        if parent != {}:
            meta_values[index] = parent
    logger.debug("String to dict complete {}".format(parent))
    logger.debug("Meta values {}".format(meta_values))
    return meta_values


def walk(node, root=False):
    logger.debug("Walking {}".format(node))
    if isinstance(node, str):
        yield node
    elif isinstance(node, list):
        for item in node:
            yield from walk(item)
    elif isinstance(node, dict):
        for key, item in node.items():
            logger.debug("{} -> {}".format(key, item))
            yield from walk(item)
    else:
        raise Exception("Unknown type in dict {}".format(type(node)))


def populate_global_depedancy_tree(provides, requires, id=None):
    logger.debug("Provides {}".format(provides))
    logger.debug("Requires {}".format(requires))

    for item in walk(provides, root=True):
        logger.debug("Provide item {}".format(item))
        logger.debug(
            "Respsonse from manager {}".format(manager.supplied.add(item)))
    for item in walk(requires, root=True):
        logger.debug("Supplies item {}".format(item))
        logger.debug(
            "Respsonse from manager {}".format(manager.required.add(item)))

    logger.debug("Id {}".format(id))
    logger.debug(manager)
    pass


def populate_with_meta(current_node, populate={}):
    """
    For each current node, get the required meta key and value.
    """
    logger.debug("Current node with meta {}".format(current_node))
    provides = get_provides(current_node, populate=populate)
    requires = get_requires(current_node, populate=populate)
    # if requires is not None:
    #     import pdb; pdb.set_trace()
    get_excludes(current_node, populate=populate)
    populate_global_depedancy_tree(provides, requires, id=populate.get("id"))


def get_data_from_catalog_item(item):
    """
    For each catalog item get the required yaml data
    id, name and meta.
    """
    eldest_child = find_eldest_child(item)
    catalog_for_yaml = {}
    catalog_for_yaml["id"] = eldest_child.find("Id").text
    catalog_for_yaml["name"] = eldest_child.find("Name").text
    meta = eldest_child.find("MetadataList")
    if meta:
        populate_with_meta(meta, populate=catalog_for_yaml)
    return catalog_for_yaml


def generate_root_catalog(catalog_item_list):
    """
    Generate the root yaml docs.

    """
    return dict(
        matrixx_product_catalog_version=0.1,
        catalog={
            "catalog_items": catalog_item_list
        }
    )


def find_all_catalog_items(data):
    """
    For a given xml document find all the required CatalogItems
    and build a yaml document, for outputting.

    Current ordered alphabetically.
    """
    attribute_defenitions = data.find_all("MtxCatalogItem")
    catalog_item_list = {}
    for catalog_item_xml_node in attribute_defenitions:
        catalog_item_dict = get_data_from_catalog_item(catalog_item_xml_node)
        item_name = "item_{}".format(catalog_item_dict["id"])
        catalog_item_list[item_name] = catalog_item_dict
    complete_dict = generate_root_catalog(catalog_item_list)

    logger.debug("Complete dictionary {}".format(complete_dict))

    return complete_dict


def output_yaml_file(complete_yaml_document, output_file=None):
    """
    Output the file to a directory/file or stdout.
    """

    if not os.path.exists(OUTPUT_DIRECTORY):
        os.makedirs(OUTPUT_DIRECTORY)

    output_path = os.path.join(OUTPUT_DIRECTORY, "result.yml")

    logger.info("Output path {}".format(output_path))
    if output_file:
        with open(output_file, "w") as yaml_file:
            yaml.dump(complete_yaml_document, yaml_file,
                      default_flow_style=False)
    else:
        print(yaml.dump(complete_yaml_document,
                        default_flow_style=False))


def load_pricing_file(filename=None):
    """
    Loads and reads the fikle into a soup.
    """
    with open(filename) as read_data:
        soup = BeautifulSoup(read_data, features="xml")
        return soup


def parse_arguments():
    """
    Parse the arugments, for configuring the program.
    """
    parser = argparse.ArgumentParser(
        description="Process a pricing file to a yaml output.")
    parser.add_argument("-i", "--input", type=str, required=True,
                        help="location of the input file pricing xml.")
    parser.add_argument("-o", "--output", type=str,
                        help="location to output the new yaml file.")
    parser.add_argument("-v", "--verbosity", type=str, default="INFO",
                        help="Logging level for application")

    args = parser.parse_args()
    logger.setLevel(args.verbosity)
    return args


def find_all_errors(soup):
    """
    Find all the errors and print them out,
    if there are some to be found and print it.
    """
    ERROR_ATTRIBUTES = [
        "MsgLevel",
        "ErrorId",
        "ContainerName",
        "Id",
        "ErrorText",
        "ErrorDetail",
        "PricingKey",
        "Path",
        "ClientPath"
    ]
    error_nodes = soup.find_all("MtxPriceLoaderError")
    if error_nodes:
        main_logger.error("Warning - Errors Found")
        for error in error_nodes:
            main_logger.error("*" * 40)
            for error_attribute in ERROR_ATTRIBUTES:
                node = error.find(error_attribute)
                if node:
                    main_logger.error(
                        "{} {}".format(
                            error_attribute, node.text))
        main_logger.error("*" * 40)


def main():
    """
    Entry point.
    """
    args = parse_arguments()
    soup = load_pricing_file(filename=args.input)
    errors = find_all_errors(soup)
    yaml_file = find_all_catalog_items(soup)
    output_yaml_file(yaml_file, output_file=args.output)
    not_found = manager.find_unsatified_depedancies()
    if not_found:
        for item in not_found:
            logger.warning(
                "Item listed as dependanies" +
                " but not found in provides \"{}\"".format(
                    item))


if __name__ == "__main__":
    main()
